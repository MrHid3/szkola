#include <iostream>
#include <cstdlib>

using namespace std;

int **tworzenie(int x, int y, int max) {
	int **tab;
	tab = new int* [x];
	for (int i = 0; i < x; i++)
		tab[i] = new int[y];

	for (int i = 0; i < x; i++)
		for (int j = 0; j < y; j++)
			tab[i][j] = rand() % (max + 1);

	return tab;
}

int** dodawanie(int **tab1, int **tab2, int x, int y) {
	int** tab;
	tab = new int* [x];
	for (int i = 0; i < x; i++)
		tab[i] = new int[y];

	for (int i = 0; i < x; i++)
		for (int j = 0; j < y; j++)
			tab[i][j] = tab1[i][j] + tab2[i][j];

	return tab;
}

int** skalarne(int** tab1, int skala, int x, int y) {
	int** tab;
	tab = new int* [x];
	for (int i = 0; i < x; i++)
		tab[i] = new int[y];

	for (int i = 0; i < x; i++)
		for (int j = 0; j < y; j++)
			tab[i][j] = tab1[i][j] * skala;

	return tab;
}

int** couchy(int** tab1, int** tab2, int x1, int y1, int x2, int y2) {
	int x, y;
	x = x1;
	y = y2;
	int** tab;
	tab = new int* [x];
	for (int i = 0; i < x; i++)
		tab[i] = new int[y];
	if (x1 == y2) {
		int wynik;
		for (int i = 0; i < y1; i++)
			for (int j = 0; j < x2; j++) {
				wynik = 0;
				for (int k = 0; k < x1; k++)
					wynik += tab1[k][i] * tab2[j][k];
				tab[j][i] = wynik;
			}
	}
	else
		return NULL;
	return tab;
}

int main() {
	srand(time(NULL));
	
	int x, y;
	cout << "x: ";
	cin >> x;
	cout << endl << "y: ";
	cin >> y;
	cout << endl;

	int x2, y2;
	cout << "x2: ";
	cin >> x2;
	cout << endl << "y2: ";
	cin >> y2;
	cout << endl;

	int** tab1;
	tab1 = new int* [x];
	for (int i = 0; i < x; i++)
		tab1[i] = new int[y];
	tab1 = tworzenie(x, y, 9);
	cout << "tab1" << endl;
	for (int i = 0; i < y; i++) {
		for (int j = 0; j < x; j++)
			cout << tab1[j][i] << "\t";
		cout << endl;
	}
	cout << endl;

	int** tab2;
	tab2 = new int* [x];
	for (int i = 0; i < x; i++)
		tab2[i] = new int[y];
	tab2 = tworzenie(x, y, 9);
	cout << "tab2" << endl;
	for (int i = 0; i < y; i++) {
		for (int j = 0; j < x; j++)
			cout << tab2[j][i] << "\t";
		cout << endl;
	}
	cout << endl;

	int** tab3;
	tab3 = new int* [x];
	for (int i = 0; i < x; i++)
		tab3[i] = new int[y];
	tab3 = dodawanie(tab1, tab2, x, y);
	cout << "tab3" << endl;
	for (int i = 0; i < y; i++) {
		for (int j = 0; j < x; j++)
			cout << tab3[j][i] << "\t";
		cout << endl;
	}
	cout << endl;

	int skala;
	cout << "skala: ";
	cin >> skala;
	cout << endl;
	int** tab4;
	tab4 = new int* [x];
	for (int i = 0; i < x; i++)
		tab4[i] = new int[y];
	tab4 = skalarne(tab3, skala, x, y);
	cout << "tab4" << endl;
	for (int i = 0; i < y; i++) {
		for (int j = 0; j < x; j++)
			cout << tab4[j][i] << "\t";
		cout << endl;
	}
	cout << endl;

	int** tab5;
	tab5 = new int* [x2];
	for (int i = 0; i < x2; i++)
		tab5[i] = new int[y2];
	tab5 = tworzenie(x2, y2, 9);
	cout << "tab5" << endl;
	for (int i = 0; i < y2; i++) {
		for (int j = 0; j < x2; j++)
			cout << tab5[j][i] << "\t";
		cout << endl;
	}
	cout << endl;

	int** tab6;
	tab6 = new int* [x];
	for (int i = 0; i < x; i++)
		tab6[i] = new int[y2];
	tab6 = couchy(tab1, tab5, x, y, x2, y2);
	cout << "tab6" << endl;
	for (int i = 0; i < y; i++) {
		for (int j = 0; j < x2; j++)
			cout << tab6[j][i] << "\t";
		cout << endl;
	}
	cout << endl;

	system("pause");
	return 0;
}