#include <iostream>

using namespace std;

void zamien(int &a, int &b){
    int pom = a;
    a = b;
    b = pom;
}

void bubbleSort(int tab[], int tabSize) {
    bool zamiana;
    for (int j = 1; j < tabSize; j++) {
        zamiana = false;
        for (int i = 0; i < tabSize - j; i++) {
            if (tab[i] > tab[i + 1]) {
                swap(tab[i], tab[i + 1]);
                zamiana = true;
            }
        } if (!zamiana) return;
    }
}

/* // Stary kod żeby Gabriel wstydził się jaki to błąd popełnił
// (Nic nie sprawdzało co jest większe a co nie)

void bubbleSort(int tab[],int n){
    bool zamiana;
    for(int j=1;j<n;j++){
        zamiana=false;
        for(int i=0;i<n-j;i++){
            zamien(tab[i],tab[i+1]);
            zamiana=true;
        }
        if (!zamiana) return;
    }
}

// Żeby bubbleSorta tak zjebać?
*/

int main(){
    int tab[30];
    for(int i=0;i<30;i++) tab[i] = (rand() % 30) + 1;
    for(int i=0;i<30;i++) cout << tab[i] << " | ";
    bubbleSort(tab, sizeof(tab));
    for(int i=0;i<30;i++){
        cout << tab[i];
    }
    return 0;
}
