#include <iostream>
#include <math.h>

using namespace std;


long long funkcja(long long liczba, int potega) {
    if (potega == 1) {
        return liczba;
    } else {
        if (potega % 2 == 0) {
            long long tak = funkcja(liczba, potega / 2);
            return tak * tak;
        } else {
            long long tak = funkcja(liczba, (potega - 1) / 2);
            return tak * tak * liczba;
        }
    }
}

long long funkcjait(long long liczba, int potega) {
    long long wynik = 1;
    while (potega > 0) {
        if (potega % 2 == 1) {
            wynik *= liczba;
        };
        liczba *= liczba;
        potega /= 2;
    }
    return wynik;
}

int main() {
    cout << funkcja(2, 7) << endl;
    cout << funkcjait(2, 7) << endl;
    return 0;
}