// Oskar Biedroń 1P 20.04.2023
// Zadanie Szyfr Vigenere'a
// C++ 17

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <iomanip>

using namespace std;

ifstream f, szyfr;
ofstream out;

/* poprawianie klucza */

int normalizeKey(string &key, const string& passphrase) {
    int itr = 1;
    string initialKey = key;
    while (key.length() <= passphrase.length()) {
        key += initialKey;
        itr++;
    }
    return itr;
}

/* szyfrowanie zad 1*/

void cipher(string key, string passphrase, string alfa[]) {
    int firstIndex, secondIndex, count = 0; // zmienna count pozbywa się zbędnego przesunięcia klucza w przypadku wystąpienia znaku specjalnego
    out << "Zaszyfrowana wiadomość: ";

    for (int i = 0; i < passphrase.length(); i++){
        if (passphrase[i] == '.') {
            out << '.';
            count++;
            continue;
        }
        if (passphrase[i] == ',') {
            out << ',';
            count++;
            continue;
        }
        if (passphrase[i] == ' ')
        {
            out << ' ';
            count++;
            continue;
        }

        for (int j = 0; j < 26; j++){
            if(alfa[0][j] == passphrase[i]){
                firstIndex = j;
            }
            if (alfa[j][0] == key[i-count]){
                secondIndex = j;
            }
        }

        out << alfa[firstIndex][secondIndex];
    }
    out << '\n';
}

/* odszyfrowywanie zad 2*/

void decipher(string passphrase, string key, string alfa[]) {
    int index, count = 0;
    out << "Odszyfrowana wiadomość: ";
    for (int i = 0; i < passphrase.length(); i++){
        if (passphrase[i] == '.') {
            out << '.';
            count++;
            continue;
        }
        if (passphrase[i] == ',') {
            out << ',';
            count++;
            continue;
        }
        if (passphrase[i] == ' ') {
            out << ' ';
            count++;
            continue;
        }

        for (int j = 0; j < 26; j++){
            if(alfa[0][j] == key[i-count]){
                index = j;
                for (int k = 0; k < 26; k++) {
                    if (alfa[k][index] == passphrase[i]) {
                        out << alfa[k][0];
                    }
                }
            }
        }
    }
    out << '\n';
}

/* znajdowanie klucza zad 3 */

void findKey(string alfa[], string passphrase) {
    int tab[26], suma = 0;
    out << "Liczba powtórzeń poszczególnych liter: \n";
    for (int i = 0; i < 26; i++){
        tab[i] = count(passphrase.begin(), passphrase.end(), alfa[0][i]);
        out << "\t" << alfa[0][i] << ": " << tab[i] << '\n';
        suma += tab[i];
    }

    double fragment1 = 0;
    for (int i : tab) {
        fragment1 += i*(i-1);
    }

    double fragment2 = fragment1 / (suma*(suma-1));
    double wynik = 0.0285 / (fragment2 - 0.0385);

    out << "Oszacowana długość klucza: " << setprecision(4) << wynik << '\n';
    szyfr.close();
}

int main(){
    string alfa[26];
    f.open("dokad.txt");
    szyfr.open("szyfr.txt");
    out.open("wyniki.txt");

    string passphrase, key = "LUBIMYCZYTAC";
    getline(f, passphrase, '\n');

    for (int i = 0; i < 26; i++){
        for (int j = 0; j < 26; j++) {
            int znak = (i+j)%26+65;
            alfa[i] += (char)znak;
        }
    }

    string key2, passphrase2;

    getline(szyfr, passphrase2, '\n'); // odczytanie wiadomości do odszyfrowania
    getline(szyfr, key2, '\n'); // odczytanie klucza

    string key3 = key2; // zapisanie wstępnej długości klucza do późniejszego zastosowania

    out << "Liczba powtórzeń klucza: " << normalizeKey(key, passphrase) << '\n'; // dopasowanie klucza do długości tekstu

    normalizeKey(key2, passphrase2); // dopasowanie klucza do długości tekstu (dla kolejnego zadania)
    cipher(key, passphrase, alfa); // szyfrowanie
    decipher(passphrase2, key2, alfa); // odszyfrowywanie
    findKey(alfa, passphrase2); // szacowanie długości klucza

    out << "Rzeczywista długość klucza: " << key3.length();

    out.close();
    szyfr.close();

    return 0;
}
