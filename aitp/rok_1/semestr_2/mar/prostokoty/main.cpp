#include <iostream>
#include <cmath>

using namespace std;

float f(float liczba) {
    return 2 * liczba;
}

float kaluza(int start, int end, int n) {
    float p = 0;
    float d = fabs(start - end) / 2;
    for (int i = 0; i < n; i++) {
        p += d * f(start + (i * d));
    }
    return p;
}

int main() {
    cout << kaluza(-5, 5, 1000);
    return 0;
}
