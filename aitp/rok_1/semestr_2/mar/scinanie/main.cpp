#include <iostream>
#include <cmath>

using namespace std;

double f(double x)
{return pow((x-10), 3);}

double cut(double a, double b, double epsilon, double n){
    double c;
    for(int i = 0; i < n; i++) {
        c = a - f(a) * ((b - a) / (f(b) - f(a)));
        b = a;
        a = c;
        if (fabs(a - b) < epsilon) return c;
    }
    return NULL;
}



int main(){
    const double epsilon = 0.000000000000000000001;
    cout << cut(100, 200, epsilon, 1000);
    return 0;
}
