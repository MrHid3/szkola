#include <iostream>
#include <string>
#include <fstream>

using namespace std;
string dzikaTablica[52] = {
"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
"bcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZa",
"cdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZab",
"defghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabc",
"efghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcd",
"fghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcde",
"ghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdef",
"hijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefg",
"ijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefgh",
"jklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghi",
"klmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghij",
"lmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijk",
"mnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijkl",
"nopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklm",
"opqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmn",
"pqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmno",
"qrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnop",
"rstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopq",
"stuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqr",
"tuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrs",
"uvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrst",
"vwxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstu",
"wxyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuv",
"xyzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvw",
"yzABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwx",
"zABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxy",
"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz",
"BCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzA",
"CDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzAB",
"DEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABC",
"EFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCD",
"FGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDE",
"GHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEF",
"HIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFG",
"IJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGH",
"JKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHI",
"KLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJ",
"LMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJK",
"MNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKL",
"NOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLM",
"OPQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMN",
"PQRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNO",
"QRSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP",
"RSTUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQ",
"STUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQR",
"TUVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRS",
"UVWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRST",
"VWXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTU",
"WXYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUV",
"XYZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVW",
"YZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWX",
"ZabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXY"
};

string zakoduj(string txt, int dlugosc, string klucz) {
    string zwrot = "";
    int x, y;
    while (klucz.size() < dlugosc)klucz += klucz;
    for(int i = 0; i < dlugosc; i++){
        x = 0;
        while (dzikaTablica[x][0] != klucz[i]){
            x++;
        }
        y = 0;
        while (txt[i] != dzikaTablica[0][y]){
            y++;
        }
        zwrot += dzikaTablica[x][y];
    }
    return zwrot;
}

string odkoduj(string txt, int dlugosc, string klucz) {
    string zwrot = "";
    int x,y;
    while (klucz.size() < dlugosc) klucz += klucz;
    for (int i = 0; i < dlugosc; i++){
        x = 0;
        while(dzikaTablica[x][0] != klucz[i]) x++;
        y = 0;
        while(dzikaTablica[x][y] != txt[i]) y++;
        zwrot += dzikaTablica[0][y];
    }
    return zwrot;
}

int main() {
    ifstream in;
    in.open("tekst.txt");
    string tekst;
    getline(in, tekst);
    in.close();
    tekst = zakoduj(tekst, 6, "ciupal");
    ofstream szyfr, czysty;
    szyfr.open("szyfrogram.txt");
    cout << tekst;
    szyfr << tekst;
    szyfr.close();
    tekst = odkoduj(tekst, 6, "ciupal");
    czysty.open("odszyfrowane.txt");
    cout << tekst;
    czysty << tekst;
    return 0;
}
