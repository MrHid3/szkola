#include <iostream>
#include <string>
#include <fstream>

using namespace std;

string alfabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

string zakoduj(string txt, int dlugosc, int klucz) {
    int j;
    for (int i = 0; i < dlugosc; i++) {
        j = -1;
        while (alfabet[j] != txt[i]) {
            j++;
            alfabet[j] = alfabet[j];
        }
        txt[i] = alfabet[(j + klucz) % 52];
    }
    return txt;
}

string odkoduj(string txt, int dlugosc, int klucz) {
    int j;
    for (int i = 0; i < dlugosc; i++) {
        j = -1;
        while (alfabet[j] != txt[i]) {
            j++;
            alfabet[j] = alfabet[j];
        }
        txt[i] = alfabet[(j + 52 - klucz) % 52];
    }
    return txt;
}

int main() {
    ifstream in;
    in.open("tekst.txt");
    string tekst;
    getline(in, tekst);
    in.close();
    tekst = zakoduj(tekst, 6, 3);
    ofstream szyfr, czysty;
    szyfr.open("szyfrogram.txt");
    szyfr << tekst;
    szyfr.close();
    tekst = odkoduj(tekst, 6, 3);
    czysty.open("odszyfrowane.txt");
    czysty << tekst;
    return 0;
}
