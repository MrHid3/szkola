#include <iostream>
#include <cmath>

using namespace std;

double f(double x){
    return pow(x - 5, 3);
}

double fals(double a, double b, double epsilon){
    double c = a - f(a)*((b-a)/(f(b) - f(a)));
    while(fabs(f(c)) > epsilon){
        if (f(a)*f(c) < 0){
            b = c;
        }
        else{
            a = c;
        }
        c = a - f(a)*((b-a)/(f(b) - f(a)));
    }
    return c;
}

int main() {
    const double epsilon = 0.000000000000000000001;
    cout << fals(-10000, 10000, epsilon);
    return 0;
}