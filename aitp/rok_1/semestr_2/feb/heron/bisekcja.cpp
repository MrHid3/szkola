#include <iostream>
#include <cmath>

using namespace std;

double f(double x){
    return pow((x-10), 3);
}

double bisekcja(double b, double a, double epsilon){
    double c;
    while(fabs(f(a)-f(b)) > epsilon || fabs(a-b) > epsilon){
        c = (a+b)/2;
        if (f(a)*f(c) < 0){
            b = c;
        }
        else{
            a = c;
        }
    }
    return c;
}

double bisekcjaRek(double b, double a, double epsilon){
    if (fabs(f(a)-f(b)) <= epsilon ) return a;
    double c = (a+b)/2;
    if (f(a)-f(b)) return bisekcjaRek(c,a,epsilon);
    else return bisekcjaRek(b,c,epsilon);
}

int main() {
    const double epsilon = 0.000000000000000000000000000000000000000000000001;
    cout << bisekcja(-9999999999999999999, 99999999999999999, epsilon);
    return 0;
}