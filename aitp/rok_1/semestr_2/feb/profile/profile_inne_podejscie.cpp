#include <iostream>
#include <string>
#include <algorithm>
#include <chrono>
#include <fstream>

float profileBubbleSort(int dlugosc, int liczbaPowtorzen);
void zamiana(int* zamianaPierwszejWartosci, int* zamianaDrugiejWartosci);
void sortBabelkowe(int dlugosc, int* tablica);
int* losowanie(int wielkoscTablicy);
float profileQuickSort(int dlugosc, int liczbaPowtorzen);
void sortSzybkie(int pierwszaPolowa, int drugaPolowa /*k-1*/, int* tablica);

std::ofstream out;

int main()
{
    remove("wyniki.txt");

    int iloscPomiarow;

    std::cout << "Podaj ilosc pomiarow do wykonania dla sortowania babelkowego (elementy tablicy co 100): ";
    std::cin >> iloscPomiarow;

    for (int i = 0; i < iloscPomiarow; ++i)
        profileBubbleSort(100*(i+1), 100);


    std::cout << "Podaj ilosc pomiarow do wykonania dla sortowania szybkiego (elementy tablicy co 100): ";
    std::cin >> iloscPomiarow;

    for (int i = 0; i < iloscPomiarow; ++i)
        profileQuickSort(100*(i+1) ,100);

    system("pause");
    return 0;
}

float profileBubbleSort(int dlugosc, int liczbaPowtorzen) {
    float wynik;
    auto* tablicaWynikow = new float[liczbaPowtorzen];
    for (int i = 0; i < liczbaPowtorzen; i++) {
        int* tablica = losowanie(dlugosc);
        auto czasPoczatkowy = std::chrono::high_resolution_clock::now();
        sortBabelkowe(dlugosc, tablica);
        auto czasKoncowy = std::chrono::high_resolution_clock::now();
        auto bubbleSortDuration = std::chrono::duration_cast<std::chrono::microseconds>(czasKoncowy - czasPoczatkowy);
        tablicaWynikow[i] = bubbleSortDuration.count();
        delete[] tablica;
    }
    std::sort(tablicaWynikow, tablicaWynikow + liczbaPowtorzen);
    wynik = 0.0f;
    for (int i = 1; i < liczbaPowtorzen - 1; i++) {
        wynik = wynik + tablicaWynikow[i];
    }
    wynik /= (float)(liczbaPowtorzen - 2.0f);
    out.open("wyniki.txt", std::ofstream::app );
    if (out.good())
        out << "Sortowanie babelkowe: " << wynik << ' ' << (dlugosc) << std::endl;
    else
        std::cout << "Blad zapisu/tworzenia pliku";
    out.close();
    delete[] tablicaWynikow;
    return wynik;
}

float profileQuickSort(int dlugosc, int liczbaPowtorzen) {
    float wynik;
    auto* tablicaWynikow = new float[liczbaPowtorzen];
    for (int i = 0; i < liczbaPowtorzen; i++) {
        int* tablica = losowanie(dlugosc);
        auto czasPoczatkowy = std::chrono::high_resolution_clock::now();
        sortSzybkie(0, dlugosc-1, tablica);
        auto czasKoncowy = std::chrono::high_resolution_clock::now();
        auto quickSortDuration = std::chrono::duration_cast<std::chrono::microseconds>(czasKoncowy - czasPoczatkowy);
        tablicaWynikow[i] = quickSortDuration.count();
        delete[] tablica;
    }
    std::sort(tablicaWynikow, tablicaWynikow + liczbaPowtorzen);
    wynik = 0.0f;
    for (int i = 1; i < liczbaPowtorzen - 1; i++)
        wynik = wynik + tablicaWynikow[i];

    wynik /= (float)(liczbaPowtorzen - 2.0f);
    out.open("wyniki.txt", std::ofstream::app );
    if (out.good())
        out << "Sortowanie szybkie: " << wynik << ' ' << (dlugosc) << std::endl;
    else
        std::cout << "Blad zapisu/tworzenia pliku";
    out.close();
    delete[] tablicaWynikow;
    return wynik;
}

void zamiana(int* zamianaPierwszejWartosci, int* zamianaDrugiejWartosci){
    int temp = *zamianaPierwszejWartosci;
    *zamianaPierwszejWartosci = *zamianaDrugiejWartosci;
    *zamianaDrugiejWartosci = temp;
}

void sortBabelkowe(int dlugosc, int* tablica){
    int i, j;
    for (i = 0; i < dlugosc - 1; i++)
        for (j = 0; j < dlugosc - i - 1; j++)
            if (tablica[j] > tablica[j + 1])
                zamiana(&tablica[j], &tablica[j + 1]);
}

int* losowanie(int wielkoscTablicy) {
    int* tablica = new int[wielkoscTablicy];
    for (int i = 0; i < wielkoscTablicy; i++)
        tablica[i] = (rand() << 16) | (rand() << 1) | (rand() & 1);
    return tablica;
}

void sortSzybkie(int pierwszaPolowa, int drugaPolowa /*k-1*/, int* tablica){
    int i = pierwszaPolowa, j = drugaPolowa;
    int pivot=tablica[(pierwszaPolowa+drugaPolowa) / 2];
    do {
        while (tablica[i]<pivot) i++;
        while (tablica[j]>pivot) j--;
        if (i<=j){
            int tmp = tablica[i];
            tablica[i]=tablica[j];
            tablica[j]=tmp;
            i++;
            j--;
        }
    } while (i <= j);
    if (pierwszaPolowa<j) sortSzybkie(pierwszaPolowa, j, tablica);
    if (i<drugaPolowa) sortSzybkie(i, drugaPolowa, tablica);
}
