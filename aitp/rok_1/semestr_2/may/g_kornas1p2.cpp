#include <iostream>
#include <string>
#include <fstream>

using namespace std;

void swap(string &word, int a, int b) {
    char tmp = word[a];
    word[a] = word[b];
    word[b] = tmp;
}

int main() {
    ifstream in;
    ofstream out;

    int klucz1[50] = {13, 2, 10, 50, 1, 28, 37, 32, 30, 46, 19, 47, 33, 41, 24, 34, 27, 42, 49, 18, 9, 48, 23, 35, 31,
                      8, 7, 12, 6, 5, 3, 22, 43, 36, 11, 40, 26, 4, 44, 17, 39, 38, 15, 14, 25, 16, 29, 20, 21, 45};
    int klucz2[15] = {13, 10, 2, 3, 1, 12, 8, 4, 5, 7, 9, 6, 15, 14, 11};
    int klucz3[6] = {6, 2, 4, 1, 5, 3};

    string word[6];

    in.open("szyfr1.txt");
    for (auto & i : word)
        getline(in, i, '\n');
    in.close();

    out.open("wyniki_szyfr1.txt");
    for (auto & i : word) {
        for (int j = 0; j < i.length(); j++)
            swap(i, klucz1[j % 50] - 1, j);
        out << i << endl;
    }
    out.close();

    in.open("szyfr2.txt");
    out.open("wyniki_szyfr2.txt");
    getline(in, word[0], '\n');

    for (int i = 0; i < word[0].length(); i++)
        swap(word[0], klucz2[i % 15] - 1, i);

    out << word[0] << endl;

    in.close();
    out.close();

    out.open("wyniki_szyfr3.txt");
    in.open("szyfr3.txt");
    getline(in, word[0], '\n');

    for (int j = word[0].length() - 1; j >= 0; j--)
        swap(word[0], klucz3[j % 6] - 1, j);

    out << word[0] << endl;

    out.close();
    in.close();

    return 0;
}
