# Cyfrowe przetwarzanie dźwięku

## Dźwięk - podstawowe pojęcia

**Dźwięk** to fala akustyczna rozchodząca się w danym ośrodku sprężystym (ciele stałym, płynie, gazie).  

W potocznym znaczeniu dźwięk to każde rozpoznawalne przez człowieka pojedyncze wrażenia słuchowew. **Dźwięk** (w rozumieniu akustyki) składa się z tonoów. Rozpoznawalne przez człowieka pojedyncze wrażenia słuchowew. **Dźwięk** (w rozumieniu akustyki) składa się z tonów.  

ośrodek stały > ciecz > gaz - zasięg  

Głośnik musi mieć tyle samo wolności ruchu z przodu i z tyłu.  
Głośnik tworzy strefę niższego lub większego ciśnienia poruszając się w którąś ze stron, co potem dochodzi do nas jako dźwięk.  

| Własność dźwięku | Definicja | jednostka
| -------------- | -------------------------------------- | ------------------ |
| głośność | różnica między najwyższym a najniższym punktem fali dźwiękowej | bel |
| częstotliwość | okres fali | Hz |
| długość trwania dźwięku | nie zgadniesz | metr |
| barwa | suma wieloktornych powtórzeń danej częstotliwości | N/A (podoba mi sie albo nie) |  

- barwa parzysta - ciepłe
- nieparzyste - zimne 'metaliczne'  

Wzmacniacz dodaje nieparzyste  
Tylko lampa elektronowa może dodać parzyste  

Dźwięk analogowy, czyli ciągły - nieskończony zakres częstotliwości  

Dźwięk cyfrowy, używamy A/C albo C/A do translacji.  
Używamy różnicy potencjału albo pojemności.  
Studyjne są dynamiczne.  

Pojęcia używane przy przetwarzaniu
- próbkowanie - jak często mierzymy amplitudę
- kwantyzacja - przyporządkowanie każdej wartości danemu napięciu
- kodowanie - zapisanie w formacie zrozumiałym dla komputera
- kompresja - duh
- konwersja - zmiana formatu pliku

Podstawowe parametry dźwięku:
- Częstotliwość próbkowania
- Rozdzielczość bitowa
- Przepłwyność danych (bitrate) częstotliwość * rozdzielczość biotwa

Avarage nie wykryje różnicy między instruentym a cyfrowymeli próbkowanie = 44,1KHz
-||- jeżeli rozdzielczość bitowa = 16

Max. 16 bitów na bluetoothie

Formaty:
1. Niekompresowane:
- AIFF,
- WAV
2. Kompresowane bezstratnie:
- ALAC,
- APe,
- CAF,
- FLAC,
- m4a,
- OGG,
- WMA
3. Kompresowane stratnie:
- AAC,
- ASF,
- ATRAC,
- CAF,
- m4a,
- mp3,
- WMA

## PCM, CD-Audio

PCM - Pulse Code Modulation - metoda reprezentacji sygnału analagowego w systemach cyfrowych. Używana jest w telekomunikacji, w cyfrowej obróbce sygnału (np. w procesach dźwięku), do przetwarzaniu obrazu, do zapisu na płytach CD i w wielu zastosowanich przemysłowych

## WAV

Został stworzyony dla windowsa, zawiera nieskompresowany format PCM. Jedna minuta ważyła ok. 10MB, 1 sekunda 127kB  

## mp3

Bardzoy stopień kompresji, 1MB na minute i 320Kb/s

## MIDI

(Musicial Instrument Digital Interface) Komunikaty sterujące cyfrowymi instrumentami elektronicznymi

## WMA

Windowsowksie mp3. Nie przyjęła się.

## FLAC

(Free Lossless Audio Codec) popularny w torrentach. Format bezstratnej kompresji dźwięku z rodziny kodeków Ogg. FOSS btw

## AC-3- Dolby Digital

Format zapisu dźwięku wielokanałowego. Duża kompresja. 0,3Mb/s

## DTS

To co powyżej, ale używane w kinach. Płatne, grube, dobre
