[to jest ukryty komentarz]: #

ten tekst sie nie zakończy
po enterze

a ten\
tak

*italiczne*\
**litewkowe**\
***włoski litewka***\

# nagłówek 1
## nagłówek 2
### nagłówek 3
#### nagłówek 4
##### nagłówek 5
###### nagłówek 6
[spacja po # jest ważna]: #

1. lista
2. uporządkowana
3. liczby
5. nie
4. muszą
5. być
6. po kolej

- lista
- nieuporządkowana

- [ ] niezrobione zadanie
- [x] zrobione zadanie

> taki blok
> > taki zagnieżdżony block

[tekst który widzisz](link://jebackielce.eu)

to jest cytowany `kod`

```
    a to jest blok kodu
```

challange zrób tabele\

| cztery | osiem |
| ---- | -------- |
| krótsze | dłuższe |

:joy: epicka emotka