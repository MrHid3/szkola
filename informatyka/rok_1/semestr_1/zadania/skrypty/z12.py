# otwieramy plik i wczytujemy  w listę
file = open('../pliki/te_same_cyfry.txt', 'r')
lista = file.read().split('\n')

repeat = 0  # liczba powtórzeń

for i in range(0, len(lista)):  # każdy element w liście
    litery1 = list(lista[i])  # dzielimy w listę cyfr
    litery1.sort()  # którą sortujemy
    for j in range(i + 1, len(lista)):  # dla wszystkich elementów po nim robimy to samo
        litery2 = list(lista[j])
        litery2.sort()
        if litery1 == litery2:  # po czym je porównujemy
            repeat += 1

print(repeat)  # i wypisujemy liczbę powtórzeń


# zamykamy plik bo wszechświat wybuchnie
file.close()