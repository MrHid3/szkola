file = open('../pliki/pary.txt')
lista = file.read().split('\n')

for i in range(len(lista)):
    lista[i] = lista[i].split(' ')
    lista[i].pop(1)

temp = []
for i in range(len(lista)):
    temp.append(int(lista[i][0]))
lista = temp

lista = list(filter(lambda x: x >= 2 and x % 2 == 0, lista))


def pierwsze(liczba: int):
    maximum = liczba + 1
    lst = [True] * maximum
    for i in range(2, int(liczba ** 0.5 + 1)):
        if lst[i]:
            for j in range(i * i, maximum, i):
                lst[j] = False
    final = []
    for i in range(2, maximum):
        if lst[i]:
            final.append(i)
    return final


def goldbach(liczba: int):
    lista = pierwsze(liczba)
    for i in range(len(lista)):
        for j in range(1, len(lista) + 1):
            if lista[i] + lista[-j] == liczba:
                return f"{lista[i]};{lista[-j]}"


for el in lista:
    print(f"{el};{goldbach(el)};", end='')
print('')