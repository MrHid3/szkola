def w(p: int, k: float):
    punkty = (p / 100) ** k
    if punkty <= 0.4:
        return 1
    elif punkty <= 0.5:
        return 2
    elif punkty <= 0.75:
        return 3
    elif punkty <= 0.85:
        return 4
    elif punkty <= 0.95:
        return 5
    else:
        return 6


print(f"{w(20, 0.5)};", end=' ')
print(f"{w(31, 0.3)};", end=' ')
print(f"{w(49, 0.2)};", end=' ')
print(f"{w(89, 1.5)};", end=' ')
