# otwarcie pliku
file = open('../pliki/co_trzecia_parzysta.txt', 'r')
lista = file.read().split(" ")  # każde słowo po spacji jest osobnym elementem w liście

# usuwamy nieparzyste
for el in lista:
    if int(el) % 2 == 1:
        lista.remove(el)

# wypisujemy co trzecią
trzy = 0
for el in lista:
    trzy += 1
    if trzy == 3:
        print(el)

file.close()
