# otwarcie pliku
file = open('../pliki/slowa.txt', 'r')
lista = file.read().split('/n')  # każda nowa linijka to osobna pozycja w liście

ass = 0  # liczba liter 'a', as już niestety jest zajęte
awords = 0  # liczba słów o liczbie liter 'a' większej niż 3

# właściwe działanie
for el in lista:
    ass = 0
    for letter in el:
        if letter == 'a':
            ass += 1
    if ass > 3:
        awords += 1

print(awords)
file.close()
