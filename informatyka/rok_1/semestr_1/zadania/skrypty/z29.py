def pierwsze(liczba: int):
    maximum = liczba + 1
    lst = [True] * maximum
    for i in range(2, int(liczba ** 0.5 + 1)):
        if lst[i]:
            for j in range(i * i, maximum, i):
                lst[j] = False
    final = []
    for i in range(2, maximum):
        if lst[i]:
            final.append(i)
    return final


lista = open('../pliki/pokrycie.txt', 'r').read().split('\n')
liczba = int(lista[0])
lista = [int(i) for i in lista[-1].split(' ')]
for el in pierwsze(liczba):
    if el not in lista:
        print('NIE')
print('TAK')
