# input
a = 4
b = 8

# funckja znajdująca wszystkie dzielinków danej liczby w danej liczbie
def dzielniki(lista: list, liczba):
    for el in lista:
        if liczba % el != 0:
            lista.remove(el)


# wszystkie liczby mniejsze od danych liczb
lista_a = [i for i in range(1, a + 1)]
lista_b = [i for i in range(1, b + 1)]

# znalezienie dzielników liczb
dzielniki(lista_a, a)
dzielniki(lista_b, b)

# znalezienie wspólnych
wspolne = []
for el in lista_a:
    for index in lista_b:
        if el == index:
            wspolne.append(el)
            break

# wypisanie
for el in wspolne:
    print(f"{el};", end="")
