# otwieramy plik
path = input("Path to file: ")
file = open(path, 'r')
lista = file.read().split('\n')

while '' in lista:
    lista.remove('')

# używamy wbudowanych funkji bo to przecież python
print(hex(int(lista[0])))
print(oct(int(lista[1])))
print(bin(int(lista[2])))

# zamykamy plik bo wszechświat wybuchnie
file.close()
