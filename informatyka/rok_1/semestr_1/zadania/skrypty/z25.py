file = open('../pliki/odwracanie_listy.txt', 'r')
lista = file.read().split('\n')
liczby = [i for i in range(int(lista[0]))]
lista.pop(0)
lista.pop(0)
lista = [el.split(' ') for el in lista]
for el in lista:
    liczby[int(el[0]):int(el[-1])] = reversed(liczby[int(el[0]):int(el[-1])])

print(liczby)