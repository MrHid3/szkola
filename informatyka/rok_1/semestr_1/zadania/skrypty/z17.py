liczba = int(input())  # liczba uczniów
lista = []

for i in range(liczba):
    temp = str(input()).split(' ')
    temp.pop(0)
    lista.append(temp)

maximum = 0
srednia = 0

for el in lista:
    for index in el:
        srednia += int(index)
    srednia = srednia / len(el)
    if srednia > maximum:
        maximum = srednia
    srednia = 0

print(round(maximum, 2))
