# otwieramy plik
file = open('../pliki/tablica.txt', 'r')
lista = file.read().split(' ')
# zamieniamy wbudowaną funkją
lista[0] = lista[0].lower()
lista[1] = lista[1].upper()
for i in range(len(lista[2:-1])):
    lista[i+2] = lista[i+2].swapcase()
    
# wypisujemy
for el in lista:
    print(el + ';', end=" ")

# zamykamy plik bo wszechświat wybuchnie
file.close()
